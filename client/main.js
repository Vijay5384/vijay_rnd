import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './main.html';
import './Product.html';

var MAP_ZOOM = 15;

Meteor.startup(() => {
  // code to run on server at startup
  GoogleMaps.load();
});


Template.map.helpers({
  geolocationError: function() {
    var error = Geolocation.error();
    return error && error.message;
  },
  mapOptions: function() {
    var latLng = Geolocation.latLng();
    // Initialize the map once we have the latLng.
    if (GoogleMaps.loaded() && latLng) {
      return {
        center: new google.maps.LatLng(latLng.lat, latLng.lng),
        zoom: MAP_ZOOM
      };
    }
  }
});

Template.map.onCreated(function() {
  GoogleMaps.ready('map', function(map) {
    var latLng = Geolocation.latLng();

    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(latLng.lat, latLng.lng),
      map: map.instance
    });
  });
});





/*




Template.hello.onCreated(function helloOnCreated() {
  // counter starts at 0
  this.counter = new ReactiveVar(0);
});




Template.hello.helpers({
  counter() {
        Meteor.Router.to("/hello1");
  },
});

Template.hello.events({
  'click button'(event, instance) {
    // increment the counter when button is clicked
    //instance.counter.set(instance.counter.get() + 1);
  /*  FlowRouter.route('/Product.html', {
  action(params) {
    FlowRouter.Blaze.toHTML('hello1');
  }
});

  //FlowRouter.Blaze.toHTML('hello1');
  //  FlowRouter.Blaze.toHTML('hello1');
    Meteor.Router.to("/hello1");

  },
});*/
